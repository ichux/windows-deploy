import os
import cStringIO
from flask import Flask, abort, send_from_directory, jsonify, url_for, render_template, make_response
from reportlab.pdfgen import canvas
import mimerender

application = Flask(__name__)
application.use_x_sendfile = True

JOIN = os.path.join
# DOWNLOADS = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), 'Downloads')
# DOWNLOADS = os.path.abspath(os.path.join(application.root_path, '..', '..', 'Downloads'))
DOWNLOADS = JOIN(os.path.expanduser('~'), 'Downloads')

mimerender.register_mime('pdf', ('application/pdf',))
mimerender = mimerender.FlaskMimeRender(global_charset='UTF-8')


@application.route('/<path:name>')
def index(name):
    reach = JOIN(DOWNLOADS, name.replace('/', os.path.sep))
    if os.path.isfile(reach) and os.access(reach, os.R_OK):
        file_ = os.path.basename(reach)
        response = send_from_directory(reach.replace(file_, ''), file_, as_attachment=True)
        response.headers['X-Sendfile'] = response.headers.get('X-Sendfile').replace(os.path.sep, '/')
        response.headers['Cache-Control'] = 'no-cache'  # experiment with this to see results
        # response.headers['X-Sendfile-Temporary'] = response.headers.pop('X-Sendfile')

        # This is for Apache: don't set Content-Length as it will be set for you
        # https://groups.google.com/forum/#!msg/modwsgi/U1r2qpCt2YE/XTqhzgPG0cEJ
        response.headers.pop('Content-Length')
        return response
    abort(404)


@application.route('/play/<path:name>')
def play(name):
    return render_template('media.html', sing=name)


@application.route('/push/<path:name>')
def pushed(name):
    return jsonify({}), 201, {'Location': url_for('index', name=name, _external=True)}


@application.route('/download')
def download():
    csv = '''"REVIEW_DATE","AUTHOR","ISBN","DISCOUNTED_PRICE"
"1985/01/21","Douglas Adams",0345391802,5.95
"1990/01/12","Douglas Hofstadter",0465026567,9.95
"1998/07/15","Timothy ""The Parser"" Campbell",0968411304,18.99
"1999/12/03","Richard Friedman",0060630353,5.95
"2004/10/04","Randel Helms",0879755725,4.50'''
    # We need to modify the response, so the first thing we
    # need to do is create a response out of the CSV string
    response = make_response(csv)
    # This is the key: Set the right header for the response
    # to be downloaded, instead of just printed on the browser
    response.headers["Content-Disposition"] = "attachment; filename=books.csv"
    return response


@application.route('/pdf/<name>')
def pdf(name):
    output = cStringIO.StringIO()

    p = canvas.Canvas(output)
    p.drawString(100, 100, 'Hello')
    p.showPage()
    p.save()

    pdf_out = output.getvalue()
    output.close()

    response = make_response(pdf_out)
    response.headers['Content-Disposition'] = "attachment; filename={name}.pdf".format(name=name)
    response.mimetype = 'application/pdf'
    return response


def render_pdf(html):
    from xhtml2pdf import pisa
    from cStringIO import StringIO
    pdf = StringIO()
    pisa.CreatePDF(StringIO(html.encode('utf-8')), pdf)
    resp = pdf.getvalue()
    pdf.close()
    return resp


@application.route('/invoice', methods=['GET'])
@mimerender(default='html', html=lambda html: html, pdf=render_pdf, override_input_key='format')
def view_invoice(invoice_id):
    html = render_template('media.html', id=invoice_id)
    return {'html': html}  # mimerender requires a dict
